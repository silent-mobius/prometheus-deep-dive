# Prometheus Deep Dive


.footer: created by Alex M. Schapelle, Vaiolabs.IO

---
# Installing Prometheus

There are several ways Prometheus can be installed:
- binary executables: pre compiled version of Prometheus that provides simple use for monitoring.(this is what we'll do)
- source code compilation: we can download OSS code of Prometheus from [github](https://github.com:/prometheus)
- manage a container : use container with Prometheus on it already.

---
# Installing Prometheus (cont)

## commands listed below
<!-- use on debian based system -->
```
# useradd -M -r -s /bin/false prometheus
# mkdir /etc/prometheus /var/lib/prometheus
# wget https://github.com/prometheus/prometheus/release/download/v2.16.0/prometheus-2.16.0.linux-amd64.tar.gz
# tar xvzf prometheus-2.16.0.linux-amd64.tar.gz
# cp prometheus-2.16.0.linux-amd64/{prometheus,promtool} /usr/local/bin
#  chown prometheus:prometheus /usr/local/bin/{prometheus,promtool}
# cp -r prometheus-2.16.0.linux-amd64/{consoles,console_libraries} /etc/prometheus/
# cp -r prometheus-2.16.0.linux-amd64/prometheus.yaml /etc/prometheus/
# chown prometheus:prometheus -R /etc/prometheus
# chown prometheus:prometheus -R /var/lib/prometheus
# echo '[Unit]
Description=Prometheus Time Series Collection and Processing Server
Wants=network-online.target
After=network-online.target
[Service]
User=prometheus
Group=prometheus
Type=simple
ExecStart=/usr/local/bin/prometheus \
--config.file /etc/prometheus/prometheus.yml \
--storage.tsdb.path /var/lib/prometheus/ \
--web.console.templates=/etc/prometheus/consoles \
--web.console.libraries=/etc/prometheus/console_libraries
[Install]
WantedBy=multi-user.target' >> /etc/systemd/system/prometheus.service
# systemctl daemon-reload
# systemctl enable --now prometheus

```
---
# Installing Prometheus (cont)

