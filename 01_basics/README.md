# Prometheus Deep Dive


.footer: created by Alex M. Schapelle, Vaiolabs.IO

---
# What is Prometheus?

Prometheus is a name of Demi-God who brought fire to the people and was punished by other Gods for it.

In the world of IT, Prometheus is an open-source `monitoring` and `alerting` tool. It collects data about systems and application, in various manners and allows to its user to `visualize` the data and issue alerts based on the data.

`Monitoring` is an important component of IT and of DevOps in particular. To manage a robust and complex infrastructure, you need to be able to grasp the situation of your environment quickly and easily, and this is where prometheus might be handy.


---
# Prometheus background

- History: developed by `Matt J. Proud` and `Julius Volz` and was part of `SoundCloud` infrastructure, and later converted to be OSS.  
- Language: mostly written in `Go`, yet it has some parts written in `Java`, `Python` and `Ruby`
- License: currently set as an [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0.html) license.
- More info: [prometheus.io](https://prometheus.io)

---
# High-Level use cases of Prometheus

- Metric Collection: collect important metrics about your apps and systems in one place.
- Visualization: build dashboards that provide an overview of the health of your systems.
- Alerting: receive an email when something is broken.

---
# Prometheus architecture

Prometheus has a lot of components, yet not all of them are required if they are not used.
Basic components for Prometheus system use, are:
- `Prometheus server` - a central server that gathers metrics and makes them available
- `Exported` - agents that expose data about systems and applications for the collection by the `Prometheus server`

---
# Prometheus architecture (cont.)

## Prometheus pull model
  
Prometheus collects metrics using a `pull model`. Meaning tha data is pulled by Prometheus, and `NOT` pushed by the agents on the remote systems or applications

---
# Prometheus architecture (cont.)

<img src:'../misc/.img/prometheus_arch.png'>

- Prometheus server: data collector
- Exporters: provides metric data for Prometheus server to consume.
- Client libraries: libraries that enable your custom apps to export data to Prometheus server.
- Prometheus push-gateway: allows pushing metrics to server for certain specific use cases.
- Alert manager: sends alerts triggered by the metric data
- Visualization Tools: provides useful way to view metric data. there are not necessary part of Prometheus.

---
# Prometheus Limitation

It is important to understand when  and when not to use prometheus. here is few suggestion when prometheus might `NOT` be the correct tool for monitoring you systems and apps:

- 100% accuracy: Prometheus is designed to operate even under failure conditions, this means it will continue to provide data even if new data not available due to failure or outage. if you need 100% accuracy, for example in case of per-request billing, then Prometheus might not be  correct tool for you.
- Non time-series data: Prometheus is built for monitor [time-series metrics](https://en.wikipedia.org/wiki/Time_series) it is not the best choice for collecting generic types of data such as system logs(log aggregation).
---
# Time-Series Data
- Prometheus is built around storing time-series data.
- Time-series data consists of a series of values associated with different points in time.

---
# Single Data Point vs. Time Series

- You could track a single data point, such as the current outdoor temperature.

- Outdoor temperature: 0C / 31F

- However, if you write down the temperature once every hour, that's a time series!

- 08:00AM — -6C / 21F
- 09:00AM — -3C / 26F
- 10:00AM — -2C / 28F
- 11:00AM — 0C / 31F

---
# Time-Series Metric Example
Every metric in Prometheus tracks a particular value over time.
For example, Prometheus might track the available memory for a server, with an
entry in the time series for every minute.
02-21-2020 09:55AM node_memory_MemAvailable_bytes=3734269952

02-21-2020 09:56AM node_memory_MemAvailable_bytes=3734276545

02-21-2020 09:57AM node_memory_MemAvailable_bytes=3734295563

02-21-2020 09:58AM node_memory_MemAvailable_bytes=3734263327

---
# Time-Series Data in Prometheus

All Prometheus data is fundamentally stored as time-series data.

This means Prometheus not only tracks the current value of each metric but also changes to each metric over time.

---
# Querying

Querying allows you to access and work with your metric data in Prometheus.

You can use PromQL (Prometheus Query Language) to write queries and retrieve useful information from the metric data collected by Prometheus.

---
# Querying

You can use Prometheus queries in a variety of ways to obtain and work with data.
- Expression browser
- Prometheus HTTP API
- Visualization tools such as Grafana

---
# Visualization

In Prometheus, visualization refers to the creation of visual representations of your metric data, such as charts, graphs, dashboards, etc.

---
# Visualization Methods
There are multiple tools that can help you visualize your metric data. Some tools are built into Prometheus.
- Expression Browser
- Console Templates

Other external tools can be integrated with Prometheus to visualize Prometheus data.
- Grafana
- Others

---
# Grafana
- Grafana is an open-source analytics and monitoring tool.

- Grafana can connect to Prometheus, allowing you to build visualizations and dashboards to display your Prometheus metric data.

---
# Grafana and Prometheus
Grafana is an open-source analytics and monitoring tool.
With Grafana, you can:
- Access Prometheus data using queries.
- Display query results using a variety of different panels (graphs, gauges, tables, etc.).
- Collect multiple panels into a single dashboard.

---
# Exporters
Exporters provide the metric data that is collected by the Prometheus server.

An exporter is any application that exposes data in a format Prometheus can read. The scrape_config in prometheus.yml configures the Prometheus server to regularly connect to each exporter and collect metric data.

---
# Prometheus Push-gateway
Prometheus server uses a pull method to collect metrics, meaning Prometheus reaches out to exporters to pull data. Exporters do not reach out to Prometheus.

However, there are some use cases where a push method is necessary, such as monitoring of batch job processes.

Prometheus Push-gateway serves as a middle-man for these use cases.
- Clients push metric data to Push-gateway.
- Prometheus server pulls metrics from Push-gateway, just like any other exporter.

---
# When to Use Push-gateway
The Prometheus documentation recommends using Push-gateway only for very specific use cases.

These usually involve service-level batch jobs. A batch job’s process exits when processing is
finished. It is unable to serve metrics once the job is complete. It should not need to wait for a scrape from Prometheus server in order to provide metrics; therefore, such jobs need a way to push metrics at the appropriate time.

---
# Recording Rules

Recording rules allow you to pre-compute the values of expressions and queries and save the results as their own separate set of time-series data.

Recording rules are evaluated on a schedule, executing an expression and saving the result as a new metric.

Recording rules are especially useful when you have complex or expensive queries that are run
frequently. For example, by saving pre-computed results using a recording rule, the expression does
not need to be re-evaluated every time someone opens a dashboard.

---
# Recording Rules

Recording rules are configured using YAML. Create them by placing YAML files in the location
specified by rule_files in prometheus.yml.

When creating or changing recording rules, reload their configuration the same way you would when
changing prometheus.yml.

```yaml
groups:
- name: my_rule_group
  rules:
  - record: my_custom_metric
    expr: up{job=“My Job”}
```

---
# Alert manager

Alert manager is an application that runs in a separate process from the Prometheus server. It is
responsible for handling alerts sent to it by clients such as Prometheus server.

Alerts are notifications that are triggered automatically by metric data.

For example: A server goes down, and the on-call administrator automatically gets an email notifying
them of the problem so they can take action.

---
# Alert manager
Alert manager does the following:
- Duplicating alerts when multiple clients send the same alert.
- Grouping multiple alerts together when they happen around the same time.
- Routing alerts to the proper destination such as email, or another alerting application such as
PagerDuty or OpsGenie.

Alert manager does not create alerts or determine when alerts needs to be sent based on metric data.
Prometheus handles that step and forwards the resulting alerts to Alert manager.

---
# Some Prometheus Server Security Considerations
- Prometheus server does not provide authentication out of the box. Anyone who can access the
server’s HTTP endpoints has access to all your time-series data.
- Prometheus server does not provide TLS encryption. Non-encrypted communications between
clients and Prometheus are vulnerable to un-encrypted data being read and to man-in-the middle
attacks.
- If your Prometheus endpoints are open to a network with potentially un-trusted clients, you can
add your own security layer on top of Prometheus server using a reverse proxy.

---
# Unsecured Traffic and Endpoints

Be sure to consider all potentially
unsecured traffic in your Prometheus
setup. If your network configuration would
allow un-trusted users to gain access to
sensitive components or data, you may
need to take steps to secure your
Prometheus setup.
<!-- need describing img -->

---
# Alert manager Security Considerations
- Like Prometheus server, Alert manager does not provide authentication or TLS encryption.
- Use a reverse proxy to add your own security layer, if needed.
Push-gateway Security Considerations
- Push-gateway likewise does not provide authentication or TLS encryption.
- Again, you can add your own security layer with a reverse proxy.

---
# Exporter Security Considerations

- Every exporter is different.
- Many exporters do provide authentication and/or TLS encryption.
- Check the documentation for your exporters to learn more about basic security features.
- Without security, data provided by exporters can be read by anyone with access to the /metrics endpoint.

---

# Client Libraries
Prometheus client libraries provide an easy way
to add instrumentation to your code in order to
monitor your applications with Prometheus.

Client libraries provide functionality that allows
you to:
- Collect and record metric data in your code.
- Provide a /metrics endpoint, turning your application into a Prometheus exporter so Prometheus can scrape metrics from your application.
  <!-- needed description img -->

---
# Client Libraries
There are existing client libraries for many popular programming languages and frameworks. You can also code your own client libraries.

Prometheus supports the following official client
libraries, although there are many third-party
client libraries for other languages.

- Go
- Java/Scala
- Python
- Ruby

---
# What’s Next?

- DevOps Monitoring Deep Dive — Learn how Prometheus can function within the context of DevOps.
- Monitoring Kubernetes with Prometheus — Use Prometheus to monitor a Kubernetes cluster.
- AIOps Essentials (Autoscaling Kubernetes with Prometheus Metrics) — Learn how to use Prometheus to auto-scale Kubernetes applications.