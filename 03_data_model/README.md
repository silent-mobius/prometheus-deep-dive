
# Prometheus Deep Dive


.footer: created by Alex M. Schapelle, Vaiolabs.IO

---
# Time-Series Data

- Prometheus is built around storing time-series data.
  - Time-series data consists of series of values associated with different in time.

---

# Example: Single Data Point VS. Time Series

You could track a single data point, such as the current outdoor temperature
Outdoor temperature: 0C

However if you write down them temperature once every hour:
- 08:00AM: 18C
- 09:00AM: 23C
- 10:00AM: 26C
- 11:00AM: 29C

---

# Time-series Metric Example
 
- Every metric in Prometheus tracks a particular value over time.
  - For example, Prometheus might track the available memory for a server, with an entry in time series for every minute:

```

```
- All Prometheus data is stored as time-series data, meaning it not only tracks the current value of each metric but also changes to each metric over time.

---

# Metric and labels

- Metric Names refer to the general feature that is being measured.
  - example: `node_cpu_seconds_total` measures the total amount of CPI time being used in CPU seconds
  - Metric Name does not point to one specific value
  - Querying specific value, most likely return list of multiple data points 

<!-- show example -->
---
# Exercise

- list metrics of : memory usage


---

# Metric and labels

- Metric Labels provide additional data in regards