# Prometheus Deep Dive


.footer: created by Alex M. Schapelle, Vaiolabs.IO

---
# About course

We'll cover most of topics covered in Prometheus documentation:
Mainly, we'll cover:
- The basics
- Installation and configuration
- Prometheus data
- Visualization
- Collecting metrics
- alerting
- Advanced concepts
- Security
- Client libraries

---

# Who Is This course for ?

- Junior/senior sysadmins or DevOps who have little to none knowledge of application monitoring
- For junior/senior developers who want to learn to monitor their stacks
- Experienced ops who need refresher


---
# About Me
<img src="./me.jpg" alt="drawing" style="float:right;width:180px;">

- Over 12 years of IT industry Experience.
- Fell in love with AS-400 UNIX system at IDF.
- 5 times tried to finish degree in computer science field
    - between each semester, I tried to take IT course at various places.
        - yes, one of them was A+.
        - yes, one of them was cisco.
        - yes, one of them was RedHat course.
        - yes, one of them was LPIC1 and Shell scripting.
        - no, others i learned alone.
        - no, not maintaining debian packages any more.
---

# About Me (cont.)
- over 7 years of sysadmin:
    - Shell scripting fanatic
    - Python developer
    - JS admirer
    - Go(lang) fallen
    - Rust fan
- 5 years of working with devops
    - Git supporter
    - Vagrant enthusiast
    - Ansible consultant
    - Container believer
    - K8s user

You can reach me on linked-in: [Alex M. Schapelle](https://www.linkedin.com/in/alex-schapelle)

---
# What is Prometheus ?
- Prometheus is an open-source monitoring and alerting tool. 
  - It collects data about **applications** and systems and allows you to visualize the data and issue alerts based on the data.
- Monitoring is an important component of DevOps automation. 
  - Prometheus allows us to collect data on a robust and complex infrastructure, which helps to take decisions when something is wrong/down.


---
# Main use cases

- Metric collection - Collect important metrics about your systems and applications in one place.
- Visualization - Build dashboards that provide an overview of the health of your systems.
- Alerting - Receive an email when something is broken.

---
# History

- Prometheus was developed at SoundCloud starting in 2012. 
  - Specifically, they identified needs that Prometheus was built to meet including: a multi-dimensional data model, operational simplicity, scalable data collection, and a powerful query language, all in a single tool.
  - The project was open-source from the beginning, and began to be used by Boxever and Docker users as well, despite not being explicitly announced.
  - Prometheus was inspired by the monitoring tool Borgmon used at Google.
- Prometheus was introduced officially to public in January 2015.
- In May 2016, the Cloud Native Computing Foundation accepted Prometheus as its second incubated project, after Kubernetes.
- Prometheus 1.0 was released in July 2016.
  -  Subsequent versions were released through 2016 and 2017, leading to Prometheus 2.0 in November 2017.
  -  In August 2018, the Cloud Native Computing Foundation announced that the Prometheus project had graduated.


---
# Architecture

The two most basic components of a Prometheus system are:
- Prometheus server - A central server that gathers metrics and makes them available.
- Exporters - Agents that expose data about systems and applications for collection by the Prometheus server.

---

# Archtecture (cont.)
Prometheus collects metrics using a **pull model**. This means the Prometheus server pulls metric data from exporters: agents do not push data to the Prometheus server.


